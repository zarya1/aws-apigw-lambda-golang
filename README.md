# aws-apigw-lambda-golang

cookiecutter project for aws api gateway + golang lambda

### Prerequisites
- [go](https://golang.org/doc/install)
- [serverless.com](https://www.serverless.com/framework/docs/getting-started/)
- [cookiecutter](https://cookiecutter.readthedocs.io/en/1.7.2/installation.html)

### Using cookiecutter to create a new aws stack project
```
cookiecutter https://gitlab.com/zarya1/aws-apigw-lambda-golang.git
```
### Deploy the aws stack (created by the above command) 
```
cd <new_aws_stack_project>
make dependencies
make deploy
```
### Remove the aws stack
```
make remove
```
### Run using docker
```
docker run -it -e AWS_ACCESS_KEY_ID=<key> -e AWS_SECRET_ACCESS_KEY=<secret> cafbr/build-sls-cookiecutter:01 bash
```
### Product Developers team: working together
![grpc stream](./bff.png)

